// Sets up pins
const int finapPin = A1; 	// Finger cuff pressure Input Pin
const int heightPin = A2; 	// Hydrostatic finger height Input Pin
const int armcufPin = A3; 	// Arm cuff pressure Input Pin
const int plethPin = A4; 	// Finger plethysmogram Input Pin

// Sets up values
int finap = 0;	// Finger cuff pressure
int height = 0; // Hydrostatic finger height
int armcuf = 0; // Arm cuff pressure
int pleth = 0; 	// Finger plethysmogram
float t_elasped = 0; // Elapsed time in milliseconds.

// Conversion units
float mV2mmHg = 0.1;
float height2mmHg = 1; // Can't seem to calibrate this so I'll just use the raw values for now.

void setup() {
  // Initializes serial communications at 9600 bps
  Serial.begin(9600);
}

void loop() {
  // Reads the analog values
  finap = analogRead(finapPin) * mV2mmHg;
  height = analogRead(heightPin) * height2mmHg;
  armcuf = analogRead(armcufPin) * mV2mmHg;
  pleth = analogRead(plethPin);

  // Gets elapsed time
  t_elasped = millis();

  // Prints to serial monitor
  Serial.print(t_elasped);
  Serial.print(",");
  Serial.print(finap);
  Serial.print(",");
  Serial.print(height);
  Serial.print(",");
  Serial.print(armcuf);
  Serial.print(",");
  Serial.println(pleth);

  // Waits 2 milliseconds before the next loop for the pins to settle after last reading.
  delay(2);

}
